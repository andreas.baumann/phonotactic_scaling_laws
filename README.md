# Scaling laws for phonotactic complexity

This project contains the code for all analyses done in the contribution:
"Scaling laws for phonotactic complexity in spoken English language data"

Cite as *Baumann, A.; Kaźmierski, K.; Matzinger, T. (in review). Scaling laws for phonotactic complexity in spoken English language data.*

## Setup

Clone with ```git clone https://gitlab.com/andreas.baumann/phonotactic_scaling_laws.git``` in the terminal or directly in RStudio (*New Project* > *Version Control* > *Git* > *Repository URL*: ```https://gitlab.com/andreas.baumann/phonotactic_scaling_laws.git```). 

All requirements are shown in ```renv.lock```. See https://rstudio.github.io/renv/articles/renv.html
for more information on ```renv``` dependency management.

## Usage

The file ```data.RData``` contains all frequency counts retrieved from the
Buckeye Speech Corpus as a binary. The script ```analysis.R``` generates
human readable xlsx files with all frequencies, a statistics table and plots for each configuration. 

Run with either ```RScript analysis.R -p within``` or ```RScript analysis.R -p acrosswithin``` in the terminal, respectively, depending on the chosen condition. See manuscript for more details on the data
and definitions of the two conditions "within" and "across/within".


